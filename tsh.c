

// min barebone tsh 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <time.h>
#define PAMAX 2046 
// 29 avril 1986, ... tele 



int main()
{
	char var_lastcommand[PAMAX];
	char target[PAMAX];

	fprintf( stderr, "Welcome to TSH\n" ); 
	fprintf( stderr, " (Esc, 27) to quit.\n" ); 
	char commandprompt[PAMAX];
	char charo[PAMAX];
	char cwd[PAMAX];
	strncpy( commandprompt, "", PAMAX );

	struct termios ot;
	if(tcgetattr(STDIN_FILENO, &ot) == -1) perror(")-");
	struct termios t = ot;
	t.c_lflag &= ~(ECHO | ICANON);
	t.c_cc[VMIN] = 1;
	t.c_cc[VTIME] = 0;
	if(tcsetattr(STDIN_FILENO, TCSANOW, &t) == -1) perror(")-");

	int a = 0;
	int gameover = 0; 
	int cmdlen = 0; 
	int debug = 0; 
	while( gameover == 0 ) 
	{
		//fprintf( stderr, "you pressed '%c' (d %d)\n", a, a );
		fprintf( stderr, "A:> %s\n", commandprompt ); 
		a = getchar(); 
		if ( a == 27 ) gameover = 1 ; 

		else if  ( a == 127 )
		{
		       strncpy( commandprompt, "" , PAMAX);
		}

		else if ( ( a == 13 ) || ( a == 10 ) )
		{
		        if ( debug == 1 )
			{
		           printf( "Char  %d \n" , a );   // 10 is enter
		           printf( "Strln %d \n" , strlen( commandprompt ) );   // 10 is enter
			}

			cmdlen = strlen( commandprompt );

			if ( ( commandprompt[0]    == 'c'  ) 
			  && ( commandprompt[1]  == 'd'  ) 
			  && ( commandprompt[2]  == ' '  ) ) 
			{
				strcpy( target, commandprompt +3 );
				fprintf( stderr, "Strip %s\n", target ); 
				chdir(  target ); 
				printf( "===\n" );
				printf( "PATH: %s\n", getcwd( cwd , PAMAX ) );
				printf( "===\n" );
				strncpy( commandprompt, "" , PAMAX);
			}


			else if ( ( commandprompt[0]    == 'p'  )
			 && ( commandprompt[1]  == 'w'  )   
			 && ( commandprompt[2]  == 'd'  )
			 && ( cmdlen == 3 ) ) 
			{
				printf( "===\n" );
				printf( "PATH: %s\n", getcwd( cwd , PAMAX ) );
				printf( "===\n" );
				strncpy( commandprompt, "" , PAMAX);
			}


			else if ( ( commandprompt[0]    == '!'  )
			 && ( commandprompt[1]  == 'h'  )   
			 && ( commandprompt[2]  == 'e'  )   
			 && ( commandprompt[3]  == 'l'  )   
			 && ( commandprompt[4]  == 'p'  )   
			 && ( cmdlen == 4+1 ) ) 
			{
				fprintf( stderr, "=== !HELP ===\n" ); 
				fprintf( stderr, "\n" ); 
				fprintf( stderr, "pwd:   shows the current path.\n" ); 
				fprintf( stderr, "cd:    change current directory.\n" ); 
				fprintf( stderr, "\n" ); 
				fprintf( stderr, "!last: shows the last command.\n" ); 
				fprintf( stderr, "!time: gives the unix epoch time.\n" ); 
				fprintf( stderr, "\n" ); 
				strncpy( commandprompt, "" , PAMAX);
			}


		        //printf("%d\n", (int)time(NULL));
			else if ( ( commandprompt[0]    == '!'  )
			 && ( commandprompt[1]  == 't'  )   
			 && ( commandprompt[2]  == 'i'  )   
			 && ( commandprompt[3]  == 'm'  )   
			 && ( commandprompt[4]  == 'e'  )   
			 && ( cmdlen == 4+1 ) ) 
			{
		                fprintf( stderr , "%d\n", (int)time(NULL));
				strncpy( commandprompt, "" , PAMAX);
			}


			else if ( ( commandprompt[0]    == '!'  )
			 && ( commandprompt[1]  == 'l'  )   
			 && ( commandprompt[2]  == 'a'  )   
			 && ( commandprompt[3]  == 's'  )   
			 && ( commandprompt[4]  == 't'  )   
			 && ( cmdlen == 4+1 ) ) 
			{
				printf( "===\n" );
				fprintf( stderr, "Last command:\n" ); 
				fprintf( stderr, "  %s\n", var_lastcommand ); 
				printf( "===\n" );
				strncpy( commandprompt, "" , PAMAX);
			}

			else if ( ( commandprompt[0]    == 'c'  ) && ( commandprompt[1]  == 'd'  )   
			 && ( cmdlen == 1+1 ) ) 
			{
			        printf( "HOME: %s\n", getenv( "HOME" ));
				chdir(  getenv( "HOME" ) ); 
				printf( "===\n" );
				printf( "PATH: %s\n", getcwd( cwd , PAMAX ) );
				printf( "===\n" );
				strncpy( commandprompt, "" , PAMAX);
			}

			else
			{
				fprintf( stderr, "Run\n" ); 
	                        strncpy( var_lastcommand, commandprompt , PAMAX);
				system( commandprompt );
				// cleanup
				strncpy( commandprompt, "" , PAMAX);
			}
		}

		else
		{
			snprintf( charo, sizeof(charo), "%s%c", commandprompt, a );
			strncpy( commandprompt, charo , PAMAX);
		}
	}

	if(tcsetattr(STDIN_FILENO, TCSANOW, &ot) == -1) perror(")-");

	return 0; 
}






